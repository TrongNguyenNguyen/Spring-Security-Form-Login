# Spring-Security-Form-Login

# Các công nghệ sử dụng:
* Spring 5.0.8.RELEASE
* Spring Security 5.0.8.RELEASE
* Maven
* Tomcat
* JDK 1.8
* Eclipse + Spring Tool Suite

# Một số thuộc tính của thẻ <form-login> gồm:

* always-use-default-target Nếu bằng true thì sẽ luôn chuyển hướng người dùng tới URL được định nghĩa ở thuộc tính default-target-url, bất kể hành động trước đó của người dùng muốn truy cập vào URL nào. Giá trị mặc định làfalse.
* authentication-failure-url Định nghĩ URL sẽ được chuyển hướng tới nếu login thất bại, Mặc định là /login?error
* authentication-failure-handler-ref Có thể sử dụng thay thế thuộc tính  authentication-failure-url, nó cho phép bạn điều khiển luồng sau khi xác thực thất bại.
* default-target-url Định nghĩa URL mặc định được chuyển hướng tới sau khi login thành công.
* authentication-success-handler-ref Có thể sử dụng để thay thế thuộc tính default-target-url và always-use-default-target, Cho phép điều khiển luồng, bổ sung hành động sau khi xác thực thành công.
* login-page URL được sử dụng để render trang login, mặc định là “/login”.
* login-processing-url URL mà Spring Security xử lý hành động login. mặc định là “/login”. Trong ví dụ này mình để là “/j_spring_security_login” thì action trong form login cũng sẽ có giá trị là “/j_spring_security_login“
* password-parameter Tên của request parameter chứa password, mặc định là “password”.
* username-parameter Tên của request parameter chứa username, mặc định là “username”
